const dgram = require('dgram');
const moment = require('moment');
const server = dgram.createSocket('udp4');

server.on('listening', () => {
  const address = server.address();
  console.log(`server listening ${address.address}:${address.port}`);
});

server.on('message', (msg, rinfo) => {
  let outputMessage = {
    "type": "UDP-in",
    "message": msg,
    "timestamp": moment().utcOffset(2).format('x'),
    "address": rinfo.address,
    "port": rinfo.port
  }
  console.log(outputMessage);
});

server.on('error', (err) => {
  console.log(`server error:\n${err.stack}`);
  server.close();
});

server.bind(41234);


