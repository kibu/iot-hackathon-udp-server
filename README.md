# IoT Hackathon - UDP server

Node.js based UDP server backbone for handling messages from NBIoT modems.

---
## Requirements

For development, you will only need Node.js installed in your environement.

### Node
- #### Node installation on Windows

  Just go on [official Node.js website](https://nodejs.org/) and download the installer.
Also, be sure to have `git` available in your PATH, `npm` might need it (You can find git [here](https://git-scm.com/)).

- #### Node installation on Ubuntu

  You can install nodejs and npm easily with apt install, just run the following commands.

      $ sudo apt install nodejs
      $ sudo apt install npm

- #### Other Operating Systems
  You can find more information about the installation on the [official Node.js website](https://nodejs.org/) and the [official NPM website](https://npmjs.org/).

If the installation was successful, you should be able to run the following command.

    $ node --version
    v8.11.3

    $ npm --version
    6.1.0

## Install

    $ git clone https://gitlab.com/kibu/iot-hackathon-udp-server.git
    $ cd iot-hackathon-udp-server
    $ npm install

## Running the project

    $ node udpserver.js
    
Server is now running and listening on the following port: 41234
All incoming messages are logged on the console.
    
## Testing the communication

You can easily imitate message sending through UDP with the free [PacketSender application](https://packetsender.com/). 
